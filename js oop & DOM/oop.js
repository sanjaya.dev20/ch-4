// super atau parent class
class Human {
  constructor(nama, alamat) {
    this.nama = nama;
    this.alamat = alamat;
  }

  introduce() {
    console.log("hi, my name is " + this.nama);
  }

  work() {
    console.log("work !");
  }
}

// programming sebagai child class yang dapat menggunakan method dari Human karena adanya pewarisan
class programming extends Human {
  constructor(nama, alamat, bahasa_pemrograman) {
    super(nama, alamat);

    this.bahasa_pemrograman = bahasa_pemrograman;
  }
  // override the method
//   introduce() {
//     super.introduce();
//     console.log("i can write ", this.bahasa_pemrograman);
//   }

  // overloading mthod yang di mana mthod introduce
  introduce(whitDetail) {
    super.introduce();
    (Array.isArray(whitDetail)) ?
    console.log("i can write ", this.bahasa_pemrograman) : console.log("wrong input");
  }

  code() {
    console.log(
      "code some",
      this.bahasa_pemrograman[
        Math.floor(Math.random() * this.bahasa_pemrograman.length)
      ]
    );
  }
}

// instance atau membuat objek baru dari kedua class
let fernanda = new Human("fernanda", "jakarta");
fernanda.introduce(); //hi, my name is fernanda
fernanda.work(); //work !

let rizky = new programming("Rizky", "lampung", [
  "javascript",
  "php",
  "python",
]);
//rizky.introduce(); //hi, my name is Rizky i can write  [ 'javascript', 'php', 'python' ]

rizky.introduce(["Ruby"]);
rizky.code();
