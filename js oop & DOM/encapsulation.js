class User {
  constructor(prompt) {
    let { email, password } = prompt;
    this.email = email;
    this.encryptedPassword = this.#encrypt(password);
  }
  //privat method
  #encrypt = (password) => {
    return `encrypted-version-of-${password}`
  }

  #decrypt = () => {
    return this.encryptedPassword.split(`encrypted-version-of-`)[1]
  }

  authentication(password) {
    return this.#decrypt() === password
  }

}

let Bot = new User({
    email: "dev@gmail.com",
    password: "Login123"
});
console.log(Bot.authentication("Login13"));
