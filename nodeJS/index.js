//mengimport module os untuk mengecek kapasitas memory yang tersedia
const os = require('os');
//import modul dari file lain untuk menggunakan function yang ada di dalam file tersebut
const luasSegitiga = require("./segitiga.js");
// import modul fs
const fs = require("fs");

const sabrina = require("./person.json");

// membaca suatu file serta penggunaan parameter utf-8 agar hasil dari function dapat di baca
const isi = fs.readFileSync("./text.txt","utf-8")
// membuat file baru yang di dalamnya terdapat tulisan asta
fs.writeFileSync("./test.txt","asta")

console.log('free memory : ',os.freemem());

console.log(luasSegitiga(20,45))

console.log(isi);

console.log(sabrina)